<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given I can sell the products:
     */
    public function iCanSellTheProducts(TableNode $table)
    {
        throw new PendingException();
    }

    /**
     * @When I add :arg1 apples to my basket
     */
    public function iAddApplesToMyBasket($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I add :arg1 papers to my basket
     */
    public function iAddPapersToMyBasket($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I add :arg1 toys to my basket
     */
    public function iAddToysToMyBasket($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then my basket contains:
     */
    public function myBasketContains(TableNode $table)
    {
        throw new PendingException();
    }

    /**
     * @When I remove :arg1 papers from my basket
     */
    public function iRemovePapersFromMyBasket($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then my invoice contains:
     */
    public function myInvoiceContains(TableNode $table)
    {
        throw new PendingException();
    }

    /**
     * @Then my gross total is :arg1
     */
    public function myGrossTotalIs($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then my tax total is :arg1
     */
    public function myTaxTotalIs($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then my net total is :arg1
     */
    public function myNetTotalIs($arg1)
    {
        throw new PendingException();
    }
}
