<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 21:58
 */

namespace Partime\Tests\Model;


use Partime\Model\Product;

class ProductTest extends \PHPUnit_Framework_TestCase
{
    public function testProductModelStoresValues()
    {
        $name = 'testProduct';
        $price = 1.03;
        $taxRate = 0.2;
        $product = new Product($name, $price, $taxRate);

        $this->assertSame($name, $product->getName());
        $this->assertSame($price, $product->getUnitPrice());
        $this->assertSame($taxRate, $product->getTaxRate());
    }
}
