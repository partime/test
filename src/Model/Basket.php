<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 21:41
 */

namespace Partime\Model;


class Basket
{
    public function __construct()
    {
        $this->rows = array();
    }

    /**
     * @param BasketRow $row
     * @return mixed
     */
    public function addRow(BasketRow $row)
    {
        $this->rows[] = $row;
    }

    /**
     * @param BasketRow $row
     * @return mixed
     */
    public function removeRow(BasketRow $row)
    {
        $this->rows = array_values(
            array_filter($this->rows, function($r) use ($row) {
                return $row !== $r;
            })
        );
    }

    /**
     * @return BasketRow[]
     */
    public function listRows()
    {
        return $this->rows;
    }
}