<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:26
 */

namespace Partime\Model;


class Invoice
{
    /**
     * @var InvoiceRow[]
     */
    private $invoiceRows;

    /**
     * @var float
     */
    private $grossTotal;

    /**
     * @var float
     */
    private $tax;

    /**
     * @var float
     */
    private $netTotal;

    /**
     * Invoice constructor.
     * @param InvoiceRow[] $invoiceRows
     * @param float $grossTotal
     * @param float $tax
     * @param float $netTotal
     */
    public function __construct($invoiceRows, $grossTotal, $tax, $netTotal)
    {
        $this->invoiceRows = $invoiceRows;
        $this->grossTotal = $grossTotal;
        $this->tax = $tax;
        $this->netTotal = $netTotal;
    }

    /**
     * @return InvoiceRow[]
     */
    public function getInvoiceRows()
    {
        return $this->invoiceRows;
    }

    /**
     * @return float
     */
    public function getGrossTotal()
    {
        return $this->grossTotal;
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @return float
     */
    public function getNetTotal()
    {
        return $this->netTotal;
    }
}