<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:27
 */

namespace Partime\Model;


class InvoiceRow
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $tax;

    /**
     * InvoiceRow constructor.
     * @param string $name
     * @param int $quantity
     * @param float $price
     * @param float $tax
     */
    public function __construct($name, $quantity, $price, $tax)
    {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getTax(): float
    {
        return $this->tax;
    }
}