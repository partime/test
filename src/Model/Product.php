<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 21:41
 */

namespace Partime\Model;


class Product
{
    /** @var string  */
    private $name;

    /** @var float  */
    private $unitPrice;

    /** @var float  */
    private $taxRate;

    /**
     * Product constructor.
     * @param string $name
     * @param float $unitPrice
     * @param float $taxRate
     */
    public function __construct($name, $unitPrice, $taxRate)
    {
        $this->name = $name;
        $this->unitPrice = $unitPrice;
        $this->taxRate = $taxRate;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @return float
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }
}