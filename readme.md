# Partime Student Test Exercise

### Baskets and Checkouts
 
There is a skeleton of models for a simple shop. 

There are two controllers one for dealing with a basket and one for creating a final invoice

There are also two behat features which show what the whole kit should be able to do when put together.

Please create your solution and commit it to the repo on a new branch with your name.

